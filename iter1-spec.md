# 0xDEADBEEF UML Editor Iteration 1 Specification
This is the specification for the first iteration of our cs420 project.

# Requirements
* Data Model
    * Serialization/Deserialization to and from file.
    * Versioning and how things are contained
    * Revert to previous version
* Window to show up, and have icons
    * There shall be a dropdown menu at the top supporting various operations.
    * There shall be a drawer of buttons allowing one to create objects in draw space
    * There shall be a divided main screen between object space, and the drawer.

# User Inteface
This section describes the user interface and components there of. There are 3 main sections in
application window, a collection of dropdown menus, tool drawer, and object space. The window
should have a layout as so. The dropdown shall be located at the top of the screen. Each dropdown
may have many options and fold out. The tool drawer shall be a column approximately one third of 
the window in size, on the left hand side of the
screen with buttons to do things. Object space shall be on the right side of the screen taking up
the remaining space.

## Dropdown Menus
The dropdown menu shall be a standard menu at the top of the window. It shall have the following 
things in Iteration 1, but not all will work. Those that should work will have (working) next to
their names.

* File, contains things related to the file
    * New, creates new diagram
    * Open, opens a diagram
	* Save, Saves the current diagram
    * Save As, Save the current diagram in a new file
    * _Sep_
    * Print, prints the current diagram on a single sheet of paper
    * _SEP_
    * Quit(Working), quit the application
* Help, contains things about help
    * Documentation, pops up an html viewer window containing a help file written in html
        * This window should be nonblocking to the main application window.
    * About(working), pops up a blocking windows with details about our project loaded from the maven file,
        see details in about window section.

### Help Viewer
The help view shall open a seperate window containing an html viewer that will display the help file which is
written in a restricted set of html with no styling. There is a feature built right into javafx to do this.

### About Window
The about window should contain the following details in order.

* Application Name or Logo
* Version(in small text)
* Authors Listed in Order
* Copyright year

## Tool Drawer
The tool drawer shall lie in a column on the left hand side of the screen taking up 1/4 to 1/3 
of the screen space by default, but may be resized. The screen resize shall be persisted between
startups. The absolute ratio of screen use should be maintained between window resizes if possible.

The tool drawer has 3 sections with a heading on each. These sections provide access to the various
drawings in UML. Each section has buttons related to it's heading. The first section contains generic
tools like selecting an object, and creating a text box. This section will be labeled with the heading, "TOOLS".
The next section contains UML things that are not relationships, this includes
Classboxes and Nodes. It shall be labeled "THINGS".
The last section shall contain the various relationship tools. It shall be labeled "RELATIONSHIPS".

Each section will contain square buttons of an indeterminite size. They shall not scale with the window, and
will be fixed at all screen resolutions. The buttons shall be laid out on a grid layout with no more than 6 to a row.
The grid layout shall scale the number of buttons on a row to fit with the size of the tool drawer. The buttons shall
have appropite icons attached to describe their function. In the first iteration, there shall just be a generic
number of buttons that do nothing in each section.

## Object Space
There are no requirements for object space during iteration 1 other than just existing in the window.

--- 

# Data Model
The data model shall consist of multiple classes that form like a tree model.
All models shall be in the package: `deadbeef.models`

UML consists of two major types. Things and relationships. Things are connected
together via relationships. Diagrams contain things and relationships.
Diagrams are versioned, and allow a user to revert to a previous version.
Every save causes a changelog to be pop up, and a new diagram placed in the version manager.

## Things
There are multiple types of things, as defined below. The types are as follows:

* Nodes
* Classboxes
* Collaborations
* Use Case
* Active Class(Option on classboxes)

All things have the following attributes on them:
* An id or UUID.
    * Shall be a serial or based on time. It must not be pure random.
* A label
* A set of mountpoints for relationships to attach to.
    * Possibly Nine or more. May be expanded by inheritance.

### Classboxes
Classboxes are a special case of a thing.
They have the following extra attributes

* Attributes defining a public interface of properties. This shall defined as a
    collection of classes. The class must have the following attributes
        * A label
        * A type
        * A comment
* Methods that may be acted upon that object. Shall be a collection of classes having
    the following attributes
         A label
        * A return type
        * A collection of parameters
            * May reuse the class used to define attributes for this.
* Extra content. Should be a string.

## Relationships
Relationships relate 2 things together. There are multiple types as defined below.

* Dependency
* Association
* Aggregation
* Composition
* Generalization

All relationships have the following attributes:

* Parent Thing Reference
* Child Thing Reference
* 3 labels at parent, center, child. Shall be strings.

The references to the the things shall support binding on to a side of a thing's point.
It should reference an id of the mountpoint.

These should support lazy construction allowing for the ids to be set first, then loading up
the actual relationships.

## Diagrams
Diagrams are the primary container of things and relationships.

All objects within a diagram must have a reference to their parent diagram.

Diagrams have the following public interfaces at least:

* getThingById(ThingId) Allowing for a thing to be gotten by it's UUID

Diagrams shall keep track of things and relationships seperately.



## DiagramVersionContainer


# Serialization and Deserialization
The diagram should be serialized via the [google gson library][gson], as [json][json].
There should be a  generic method used for serialization.

The things should be loaded first, followed by relationships.

The serialized data may or may not be compressed. It may be preferrable to compress it using
the built in java utilities.

[gson]: https://github.com/google/gson
[json]: https://www.json.org/

# 


## Controllers
Controller classses should be applied librally and often.

# 4 controllers
* main
* toolbar
* drawer
* object space

# main
* other 3 controllers will be children
* each controller will have an .fxml file associated with it
*

