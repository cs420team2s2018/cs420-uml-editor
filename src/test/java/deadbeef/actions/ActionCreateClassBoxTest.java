package deadbeef.actions;

import deadbeef.models.Diagram;
import deadbeef.models.DiagramModel;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Creates a class box and adds it to the diagram and the diagram model
 */

public class ActionCreateClassBoxTest {

    /**
     * Tests that a new class box is successfully created
     */

    @Test
    public void testCreation(){
        ActionCreateClassBox cb = new ActionCreateClassBox(25, 25);

        assertNotNull(cb);
    }

    /**
     * Tests that the new class box is added to the diagram and diagram model
     */
    @Test
    public void testPerform() {
        Diagram d = new Diagram();
        DiagramModel model = d.getModel();

        d.performAction((new ActionCreateClassBox(1, 2)));
        assertEquals(1, model.getThingCount());
    }

    /**
     * Tests that the new class box is created, then deleted from the
     * diagram and diagram model
     */
    @Test
    public void testPerformThenRevert() {
        Diagram d = new Diagram();
        DiagramModel model = d.getModel();

        d.performAction(new ActionCreateClassBox(1,2));
        assertEquals(1, model.getThingCount());

        d.undo();
        assertEquals(0, model.getThingCount());
    }

    /**
     * Tests that the new class box is created, then deleted, then added back
     * to the diagram and diagram model
     */
    @Test
    public void testPerformThenUndoThenRedo() {
        Diagram d = new Diagram();
        DiagramModel model = d.getModel();

        d.performAction(new ActionCreateClassBox(1, 2));
        assertEquals(1, model.getThingCount());

        d.undo();
        assertEquals(0, model.getThingCount());

        d.redo();
        assertEquals(1, model.getThingCount());
    }

    /**
     * TODO: Tests that the class box cannot be redone if the redo stack is modified after deleting the class box
     */
    @Test
    public void testInvalidateRedoStack() {
        Diagram d = new Diagram();
        DiagramModel model = d.getModel();

        d.performAction(new ActionCreateClassBox(1, 2));
    }


}
