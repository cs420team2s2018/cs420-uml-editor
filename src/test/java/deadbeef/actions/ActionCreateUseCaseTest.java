package deadbeef.actions;

import deadbeef.models.Diagram;
import deadbeef.models.DiagramModel;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Creates a use case and adds it to the diagram and the diagram model
 */

public class ActionCreateUseCaseTest {

    /**
     * Tests that a new use case is successfully created
     */
    @Test
    public void testCreation() {
        ActionCreateUseCase uc = new ActionCreateUseCase(25, 25);

        assertNotNull(uc);
    }

    /**
     * Tests that the new use case is added to the diagram and diagram model
     */
    @Test
    public void testPerform() {
        Diagram d = new Diagram();
        DiagramModel model = d.getModel();

        d.performAction((new ActionCreateUseCase(1, 2)));
        assertEquals(1, model.getThingCount());
    }

    /**
     * Tests that the new use case is created, then deleted from the
     * diagram and diagram model
     */
    @Test
    public void testPerformThenRevert() {
        Diagram d = new Diagram();
        DiagramModel model = d.getModel();

        d.performAction(new ActionCreateUseCase(1, 2));
        assertEquals(1, model.getThingCount());

        d.undo();
        assertEquals(0, model.getThingCount());
    }

    /**
     * Tests that the new use case is created, then deleted, then added back
     * to the diagram and diagram model
     */
    @Test
    public void testPerformThenUndoThenRedo() {
        Diagram d = new Diagram();
        DiagramModel model = d.getModel();

        d.performAction(new ActionCreateUseCase(1, 2));
        assertEquals(1, model.getThingCount());

        d.undo();
        assertEquals(0, model.getThingCount());

        d.redo();
        assertEquals(1, model.getThingCount());
    }

    /**
     * TODO: Tests that the use case cannot be redone if the redo stack is modified after deleting the use case
     */
    @Test
    public void testInvalidateRedoStack() {
        Diagram d = new Diagram();
        DiagramModel model = d.getModel();

        d.performAction(new ActionCreateUseCase(1, 2));
    }
}