package deadbeef.actions;

import com.rits.cloning.Cloner;
import deadbeef.models.DiagramModel;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;


public class ActionCreateRelationshipTest {
    DiagramModel d;
    ActionCreateRelationship action;
    ActionCreateClassBox cb1;
    ActionCreateClassBox cb2;

    @Before
    public void setUp() {
        d = new DiagramModel();
        action = new ActionCreateRelationship();
        cb1 = new ActionCreateClassBox(1, 1);
        cb2 = new ActionCreateClassBox(200, 200);

        action.setFromThing(cb1.getClassBox());
        action.setToThing(cb2.getClassBox());

        cb1.perform(d);
        cb2.perform(d);

        assertEquals(2, d.getThingCount());
    }

    @Test
    public void testPerformRevert() {
        Cloner cloner = new Cloner();
        DiagramModel preChangeModel = cloner.deepClone(d);

        action.perform(d);
        assertEquals(preChangeModel.getRelationshipCount() + 1, d.getRelationshipCount());
        assertEquals(preChangeModel.getThingCount(), d.getThingCount());

        action.revert(d);
        assertEquals(0, d.getRelationshipCount());
        assertReflectionEquals(preChangeModel, d);
    }

    @Test(expected = IllegalStateException.class)
    public void invalidStatePerformAction () {
        DiagramModel d = new DiagramModel();
        ActionCreateRelationship action = new ActionCreateRelationship();
        action.perform(d);
    }
}