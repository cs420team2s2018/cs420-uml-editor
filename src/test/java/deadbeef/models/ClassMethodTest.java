package deadbeef.models;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Creates a method that allows parameters defined in ClassProperty
 * and returns a string representation of user-defined methods stored
 * in ClassMethod
 */

public class ClassMethodTest {

    /**
     * Tests the results from the formatted string
     * for a method with no parameters, names, or types
     */
    @Test
    public void testGetFormattedBlankCtor() {
        ClassMethod cm = new ClassMethod();

        assertEquals("", cm.getFormatted());
    }

    /**
     * Tests the results from the formatted string
     * for a method with no parameters
     */
    @Test
    public void testGetFormattedNormalCtor() {
        ClassMethod cm = new ClassMethod("test", "void");

        assertEquals("test(): void", cm.getFormatted());
    }

    /**
     * Tests the results from the formatted string
     * for several methods with no parameters
     */
    @Test
    public void testGetFormattedMultiple() {
        ClassMethod cm1 = new ClassMethod("test1", "void");
        ClassMethod cm2 = new ClassMethod("test2", "void");

        assertEquals("test1(): void", cm1.getFormatted());
        assertEquals("test2(): void", cm2.getFormatted());
    }

    /**
     * Tests the results from the formatted string
     * for a method with a single parameter
     */
    @Test
    public void testAddParameterSingle() {
        ClassProperty cp = new ClassProperty ("var", "type");
        ClassMethod cm = new ClassMethod("test", "void");

        cm.addParameter(cp);

        assertEquals("test(var: type): void", cm.getFormatted());
    }

    /**
     * Tests the results from the formatted string
     * for a method with multiple parameters
     */
    @Test
    public void testAddParameterMultiple() {
        ClassProperty cp1 = new ClassProperty("var1", "string");
        ClassProperty cp2 = new ClassProperty("var2", "int");

        ClassMethod cm = new ClassMethod("test", "void");

        cm.addParameter(cp1);
        cm.addParameter(cp2);

        assertEquals("test(var1: string, var2: int): void", cm.getFormatted());
    }

}