package deadbeef.models;

import org.junit.Test;

import static org.junit.Assert.*;

public class DiagramModelTest {
    @Test
    public void defaultCtor() {
        DiagramModel m = new DiagramModel();
        assertEquals(0, m.getThingCount());
        assertEquals(0, m.getRelationshipCount());
    }

    @Test
    public void addThing() {
        DiagramModel m = new DiagramModel();

        m.addThing(new UmlClassBox());

        assertEquals(1, m.getThingCount());
    }
}