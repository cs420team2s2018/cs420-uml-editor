package deadbeef.models;

import javafx.scene.canvas.Canvas;
import org.junit.Test;

import static org.junit.Assert.*;

public class UmlUseCaseTest {

    @Test
    public void TestGetUseCase() {
        UmlUseCase useCase = new UmlUseCase();

        assertEquals(50.0, useCase.getHeight(),0.0);
        assertEquals(200.0, useCase.getWidth(), 0.0);
        assertEquals("new usecase", useCase.getLabel());


    }
}