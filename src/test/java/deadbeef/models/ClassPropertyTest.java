package deadbeef.models;

import org.junit.Test;
import org.junit.experimental.theories.suppliers.TestedOn;

import static org.junit.Assert.*;

/**
 * Creates a property that can be used as parameters in ClassMethod
 * objects and returns a string representation of a name and type
 * stored in ClassProperty
 */


public class ClassPropertyTest {

    /**
     * Tests the name and type assigned to a
     * new ClassProperty object
     */
    @Test
    public void ctorClassPropertyNormal() {
        String testName = "property";
        String testType = "string";
        ClassProperty classProperty = new ClassProperty(testName, testType);

        assertEquals(testName, classProperty.getName());
        assertEquals(testType, classProperty.getType());
    }

    /**
     * Tests a new ClassProperty object that does
     * not have a name or type defined
     */
    @Test
    public void ctorClassPropertyDefault() {
        ClassProperty cp = new ClassProperty();

        assertEquals("", cp.getName());
        assertEquals("", cp.getType());
    }

    /**
     * Tests the result of an empty ClassProperty object formatted
     * into a string
     */

    @Test
    public void getFormattedBlankFields() {
        ClassProperty cp = new ClassProperty();

        assertEquals("", cp.getFormatted());
    }

    /**
     * Tests the result of a ClassProperty object formatted
     * into a string
     */
    @Test
    public void getFormattedFilledOut_1() {
        ClassProperty cp = new ClassProperty("test", "string");

        assertEquals("test: string", cp.getFormatted());
    }

    /**
     * Tests the result of multiple empty ClassProperty objects
     * formatted into a string
     */
    @Test
    public void getFormattedFilledOut_2() {
        ClassProperty cp2 = new ClassProperty("test2", "int");

        assertEquals("test2: int", cp2.getFormatted());
    }

    /**
     * Tests the result of multiple ClassProperty objects
     * formatted into a string
     */
    @Test
    public void getFormattedEmptyType() {
        ClassProperty cp1 = new ClassProperty("test", "");

        assertEquals("test", cp1.getFormatted());
    }

    /**
     * Tests the results of both empty and non-empty ClassProperty
     * objects formatted into a string
     */
    @Test
    public void getFormattedNoNameFilledOutType() {
        String name = "";
        String type = "int";
        ClassProperty typed = new ClassProperty(name, type);

        assertEquals("", typed.getName());
        assertEquals(name, typed.getName());
        assertEquals(type, typed.getType());
    }

    @Test
    public void getFormattedWhiteSpace() {
        ClassProperty whitespace = new ClassProperty("   test   ", "  int ");

        assertEquals("test: int", whitespace.getFormatted());
    }
}