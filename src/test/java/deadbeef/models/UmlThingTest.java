package deadbeef.models;

import javafx.scene.canvas.GraphicsContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UmlThingTest {
    UmlThing thing;

    @Before
    public void setUp() {
        thing = new UmlThing("", 20, 20) {
            @Override
            public void draw(GraphicsContext gc) {
                // do nothing
            }
        };
        thing.setPosition(0, 0);
    }

    @After
    public void tearDown() {
        thing = null;
    }

    @Test
    public void clickActivation() {
        assertTrue(thing.clickAffectsThisObject(1, 1));
        assertTrue(thing.clickAffectsThisObject(10, 10));
        assertTrue(thing.clickAffectsThisObject(1, 10));
        // On The Edge
        assertTrue(thing.clickAffectsThisObject(20, 20));


        assertFalse(thing.clickAffectsThisObject(-1, -1));
        assertFalse(thing.clickAffectsThisObject(40, 40));
    }

    @Test
    public void clickActivationOnTheEdge() {
        UmlThing t = new UmlThing("", 20, 20) {
            @Override
            public void draw(GraphicsContext gc) {
                // do nothing
            }
        };

        //! TODO: TEST CLICKING ON THE EDGE. IT SHOULD ACTIVATE ON THE EDGE
    }
}