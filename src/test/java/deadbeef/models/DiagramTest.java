package deadbeef.models;

import deadbeef.actions.ActionCreateUseCase;
import deadbeef.actions.nop.TDataActionEvent;
import deadbeef.actions.nop.TestingActionEvent;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

public class DiagramTest {
    /**
     * Creates a testing diagram with a currentActions stack of size
     *  composed of TestingActionEvent
     * @param size indicates the size of the diagram
     * @return
     */
    private static Diagram getTestingDiagramOfSize(int size) {
        Diagram d = new Diagram();
        for (int i = 0; i < size; ++i) {
            d.performAction(new TestingActionEvent());
        }
        return d;
    }

    /**
     *
     */
    @Test
    public void testCountEffectiveActions() {
        final int NUM_ACTIONS_EFFECTIVE = 4;

        Diagram d = getTestingDiagramOfSize(NUM_ACTIONS_EFFECTIVE);
        assertEquals(NUM_ACTIONS_EFFECTIVE, d.getCurrentActionCount());

        d.undo();
        assertEquals(NUM_ACTIONS_EFFECTIVE - 1, d.getCurrentActionCount());
        assertEquals(1, d.getRevertedActionCount());

        d.redo();
        assertEquals(NUM_ACTIONS_EFFECTIVE, d.getCurrentActionCount());
        assertEquals(0, d.getRevertedActionCount());
    }

    /**
     *
     */
    @Test
    public void testInvalidateRedoStack() {
        final int INIT_NUM_ACTIONS = 4;

        Diagram d = getTestingDiagramOfSize((INIT_NUM_ACTIONS));
        d.undo();

        assertEquals(INIT_NUM_ACTIONS - 1, d.getCurrentActionCount());
        assertEquals(1, d.getRevertedActionCount());

        d.performAction(new TestingActionEvent());
        assertEquals(4, d.getCurrentActionCount());
        assertEquals(0, d.getRevertedActionCount());
    }

    /**
     *
     */
    @Test
    public void testSerializeSimple() {
        Diagram out = getTestingDiagramOfSize(2);
        String json = out.serializeToJSON();

        Diagram in = Diagram.fromJson(json);

        assertReflectionEquals(out, in);
    }

    /**
     *
     */
    @Test
    public void testSerializeDifferentTypes1() {
        Diagram out = new Diagram();

        out.performAction(new TestingActionEvent());
        out.performAction(new TDataActionEvent("data 1"));
        out.performAction(new TDataActionEvent("data 2"));

        String json = out.serializeToJSON();

        Diagram in = Diagram.fromJson(json);

        assertReflectionEquals(in, out);
    }

    /**
     *
     */
    @Test
    public void testSerializeWithRedo1() {
        Diagram out = getTestingDiagramOfSize(4);
        out.undo();
        String json = out.serializeToJSON();

        Diagram in = Diagram.fromJson(json);

        assertReflectionEquals(out, in);
    }

    /**
     *
     */
    @Test
    public void GetSetFile() {
        Diagram d = new Diagram();
        assertNull(d.getFile());

        File f = new File("");
        d.setFile(f);

        assertReflectionEquals(f, d.getFile());
    }

    @Test
    public void clickAffectsOneObject() {
        Diagram d = new Diagram();
        d.performAction(new ActionCreateUseCase(100, 200));

        // click 4 directions outside of the use case
        assertNull(d.getThingClickAffects(0, 150));
        assertNull(d.getThingClickAffects(350, 150));
        assertNull(d.getThingClickAffects(150, 350));
        assertNull(d.getThingClickAffects(0, 250));

        // Now test it on the corners
        // Use cases are 200 by 50 units
        double leftX = 100, rightX = 300, topY = 250, bottomY = 200;

        assertNotNull(d.getThingClickAffects(leftX, bottomY));
        assertNotNull(d.getThingClickAffects(leftX, topY));
        assertNotNull(d.getThingClickAffects(rightX, bottomY));
        assertNotNull(d.getThingClickAffects(rightX, topY));

        // Finally right smack dab in the middle of the object
        assertNotNull(d.getThingClickAffects(200, 225));
    }

    @Test
    public void attemptPushNullAction() {
        Diagram d = new Diagram();
        d.performAction(null);
    }

    @Test
    public void clickAffectsOverlappingObjects() {
        // TODO: Add a test for overlapping objects. The most recently placed item
        // should be the selected object
    }
}
