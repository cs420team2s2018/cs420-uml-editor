package deadbeef;

import deadbeef.controllers.MainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application {
    private static MainController controller;

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(
                getClass().getClassLoader()
                        .getResource("deadbeef-uml.fxml"));
        Parent root = loader.load();
        controller = loader.getController();
        controller.setStage(primaryStage);

        primaryStage.setTitle("0xDEADBEEF UML Editor");
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.show();
    }

    public static MainController getController() {
        return controller;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
