package deadbeef.actions.nop;

import deadbeef.actions.ActionEvent;
import deadbeef.models.DiagramModel;

// needs no documentation
public class TDataActionEvent extends ActionEvent {
    String data;

    public TDataActionEvent(String data) {
        this.data = data;
    }

    @Override
    public void perform(DiagramModel d) {}

    @Override
    public void revert(DiagramModel d) {}
}
