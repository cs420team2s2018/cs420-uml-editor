package deadbeef.actions.nop;

import deadbeef.actions.ActionEvent;
import deadbeef.models.DiagramModel;

public class TestingActionEvent extends ActionEvent {
    private int testing = 5;

    @Override
    public void perform(DiagramModel d) {

    }

    @Override
    public void revert(DiagramModel d) {

    }
}
