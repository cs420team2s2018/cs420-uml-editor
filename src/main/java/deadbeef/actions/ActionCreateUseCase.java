package deadbeef.actions;

import deadbeef.models.DiagramModel;
import deadbeef.models.UmlUseCase;

/**
 * This event is created when the user creates a use case box in the diagram
 */
public class ActionCreateUseCase extends ActionEvent {
    private UmlUseCase useCase;

    /**
     * Creates a use case and places it at the passed coordinates
     * @param x top left x-coordinate of the new use case
     * @param y top left y-coordinate of the new use case
     */
    public ActionCreateUseCase(double x, double y) {
        useCase = new UmlUseCase();
        useCase.setPosition(x, y);
    }

    /**
     * Adds the new use case to the diagram model
     * @param d Diagram Model to act on
     */
    public void perform(DiagramModel d) {
        d.addThing(useCase);
    }

    /**
     * Reverts the diagram model and deletes the created use case
     * @param d Diagram model to act on.
     */
    public void revert(DiagramModel d) {
        d.removeThingById(useCase.getUUID());
    }
}
