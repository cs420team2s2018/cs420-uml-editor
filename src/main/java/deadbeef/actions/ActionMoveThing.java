package deadbeef.actions;

import deadbeef.models.DiagramModel;
import deadbeef.models.UmlThing;

import java.util.UUID;

/**
 * An action where one wants to move a thing on screen.
 *
 * This one requires a little bit more effort to be valid.
 * It must have the new position be set, before perform is
 * called.
 */
public class ActionMoveThing extends ActionEvent {
    private UUID selectedThing;
    private double oldX, oldY, newX, newY;

    /**
     * Moves a thing in the diagram from an old coordinate to a new coordinate
     * @param thing thing to be moved
     */
    public ActionMoveThing (UmlThing thing) {
        this.selectedThing = thing.getUUID();
        this.oldX = thing.getX();
        this.oldY = thing.getY();
    }

    /**
     * Sets the position of thing to be moved to on perform.
     * @param newX the new x-coordinate of the moved thing
     * @param newY the new y-coordinate of the moved thing
     */
    public void setNewPosition(double newX, double newY) {
        this.newX = newX;
        this.newY = newY;
    }

    /**
     * Performs the movement of a thing in the diagram to another position
     *
     * @param d DiagramModel to act on
     */
    @Override
    public void perform(DiagramModel d) {
        UmlThing t = d.getThingById(selectedThing);
        t.setXPosition(newX);
        t.setYPosition(newY);
    }

    /**
     * Moves thing back to where it was before performed was called
     *
     * @param d Diagram model to act on.
     */
    @Override
    public void revert(DiagramModel d) {
        UmlThing t = d.getThingById(selectedThing);
        t.setXPosition(oldX);
        t.setYPosition(oldY);
    }
}
