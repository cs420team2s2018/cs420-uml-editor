package deadbeef.actions;

import deadbeef.models.DiagramModel;
import deadbeef.models.UmlClassBox;

/**
 * This event is created when the user creates a class box in the diagram
 */
public class ActionCreateClassBox extends ActionEvent {
    private UmlClassBox classBox;

    /**
     * Creates a new class box and places it at the passed coordinates
     * @param x top left x-coordinate of the new class box
     * @param y top left y-coordinate of the new class box
     */
    public ActionCreateClassBox(double x, double y) {
        classBox = new UmlClassBox();
        classBox.setPosition(x, y);
    }

    /**
     * Returns a specific class box
     * @return
     */
    public UmlClassBox getClassBox() {
        return classBox;
    }

    /**
     * Adds the new class box to the diagram model
     * @param d Diagram Model to act on
     */
    public void perform(DiagramModel d) {
        System.out.println("Performing ActionCreateClassBox");
        d.addThing(classBox);
    }

    /**
     * Reverts the diagram model and deletes the created class box
     * @param d Diagram model to act on.
     */
    public void revert(DiagramModel d) {
        d.removeThingById(classBox.getUUID());
    }
}
