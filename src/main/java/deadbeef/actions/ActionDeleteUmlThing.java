package deadbeef.actions;

import deadbeef.models.DiagramModel;
import deadbeef.models.UmlRelationship;
import deadbeef.models.UmlThing;

import java.util.ArrayList;
import java.util.Stack;

/**
 * This class deletes a thing object
 */

public class ActionDeleteUmlThing extends ActionEvent {
    private UmlThing thing = null;
    private transient Stack<UmlRelationship> removedRelationships;

    /**
     * Deletes a selected thing from the diagram and the diagram model
     * @param thing thing to be deleted
     */
    public ActionDeleteUmlThing(UmlThing thing) {
        this.thing = thing;
        this.removedRelationships = new Stack<>();
    }

    /**
     * Performs the changes stored in this event to the passed model.
     *
     * @param d DiagramModel to act on
     */
    @Override
    public void perform(DiagramModel d) {
        for (UmlRelationship relationship : d.getRelationshipSet()) {
            if (relationship.getFromThing() == thing || relationship.getToThing() == thing) {
                removedRelationships.push(relationship);
                d.removeRelationshipById(relationship.getUUID());
            }
        }
        d.removeThingById(thing.getUUID());
    }

    /**
     * Reverts changes that the event caused on a model
     *
     * @param d Diagram model to act on.
     */
    @Override
    public void revert(DiagramModel d) {
        d.addThing(thing);
        while(removedRelationships.size() > 0) {
            d.addRelationship(removedRelationships.pop());
        }
    }
}
