package deadbeef.actions;

import deadbeef.models.DiagramModel;

/**
 * Defines an event happening on the diagram model.
 *
 * They are tracked by deadbeef.models.Actions
 */
public abstract class ActionEvent {
    /**
     * Performs the changes stored in this event to the passed model.
     *
     * @param d DiagramModel to act on
     */
    public abstract void perform(DiagramModel d);

    /**
     * Reverts changes that the event caused on a model
     *
     * @param d Diagram model to act on.
     */
    public abstract void revert(DiagramModel d);

    /**
     * @return true if the fields are filled out to make this a valid event
     */
    public boolean valid() {
        return true;
    }
}
