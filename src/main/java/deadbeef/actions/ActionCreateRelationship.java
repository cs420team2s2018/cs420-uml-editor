package deadbeef.actions;

import deadbeef.models.DiagramModel;
import deadbeef.models.RelationshipType;
import deadbeef.models.UmlRelationship;
import deadbeef.models.UmlThing;

import java.util.UUID;

/**
 * This event is created when the user creates a relationship in the diagram
 */
public class ActionCreateRelationship extends ActionEvent {
    private transient UUID relationshipId;
    private UUID toThingId;
    private UUID fromThingId;

    /**
     * Creates an empty relationship
     */
    public ActionCreateRelationship() {
        relationshipId = null;
        toThingId = null;
        fromThingId = null;
    }

    /**
     * Sets the child thing of the relationship
     * @param thing child thing
     */
    public void setToThing(UmlThing thing) {
        this.toThingId = thing.getUUID();
    }

    /**
     * Sets the parent thing of the relationship
     * @param thing parent thing
     */
    public void setFromThing(UmlThing thing) {
        this.fromThingId = thing.getUUID();
    }

    @Override
    public boolean valid() {
        return toThingId != null && fromThingId != null;
    }

    /**
     * Performs the changes stored in this event to the passed model.
     *
     * @param d DiagramModel container object to act on
     */
    @Override
    public void perform(DiagramModel d) {
        if (! valid())
            throw new IllegalStateException("Invalid Relationship Creation Action");
        UmlRelationship relationship = new UmlRelationship(
                d.getThingById(fromThingId),
                d.getThingById(toThingId),
                RelationshipType.PLAIN);
        this.relationshipId = relationship.getUUID();
        d.addRelationship(relationship);
    }

    /**
     * Reverts changes that the event caused on a model
     *
     * @param d Diagram model to act on.
     */
    @Override
    public void revert(DiagramModel d) {
        assert relationshipId != null;
        d.removeRelationshipById(relationshipId);
    }
}
