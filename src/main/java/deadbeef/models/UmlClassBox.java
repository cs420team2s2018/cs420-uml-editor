package deadbeef.models;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

import java.util.ArrayList;
import java.util.Collection;

import static javafx.scene.input.MouseEvent.MOUSE_CLICKED;
import static javafx.scene.input.MouseEvent.MOUSE_DRAGGED;

/**
 * This class stores class boxes and their properties
 */

public class UmlClassBox extends UmlThing {
    // subclass will save classMethods and classProperties in a collection

    //private String classAttribute;
    //private String classMethod;
    //private String classExtraDetails;
    //private int[] mountPoint;
    Collection<ClassProperty> classProperties = new ArrayList<>();
    Collection<ClassMethod> classMethods = new ArrayList<>();


    /**
     * Specifies label, width, and height of the classbox being
     * created. Also prints out a message stating the class box
     * has been created.
     */
    public UmlClassBox() {
        super("new classbox", 200, 300);
        System.out.println ("new classbox created");
    }

    /**
     * Adds a new class property to the class box
     * @param property property to be added
     */
    public void addProperty(ClassProperty property) {
        classProperties.add(property);
    }

    /**
     * Adds a new class method to the class box
     * @param method method to be added
     */
    public void addMethod(ClassMethod method) {
        classMethods.add(method);
    }


    public void draw(final GraphicsContext context) {
        context.setStroke(Color.BLACK);

        String label = getLabel();
        double textHeight = context.getFont().getSize();
        context.strokeText(label, getX(), getY() + textHeight, getWidth());
        context.strokeLine(getX(), getY() + textHeight, getX() + getWidth(), getY() + textHeight);

        double propTextBegin = textHeight*2;
        for (ClassProperty property: classProperties) {
            context.strokeText(property.getFormatted(), getX(), getY() + propTextBegin, getWidth());
            propTextBegin = propTextBegin + textHeight;
        }
        context.strokeLine(getX(), getY() + ((classProperties.size() + 2)*textHeight), getX() + getWidth(),
                getY() + ((classProperties.size() + 2)*textHeight));

        double methodTextBegin = textHeight*classProperties.size();
        for (ClassMethod method: classMethods) {
            context.strokeText(method.getFormatted(), getX(), getY() + methodTextBegin, getWidth());
            methodTextBegin = methodTextBegin + textHeight;
        }
        context.strokeLine(getX(), getY() + ((classProperties.size() + classMethods.size() + 3)*textHeight),
                getX() + getWidth(), getY() + ((classProperties.size() + classMethods.size() + 3)*textHeight));

        context.strokeText("Extras", getX(),
                getY() + ((classProperties.size() + classMethods.size() + 4)*textHeight), getWidth());

        context.strokeRect(getX(), getY(), getWidth(), ((classProperties.size() + classMethods.size() + 6)*textHeight));
    }
}
