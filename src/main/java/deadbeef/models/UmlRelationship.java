package deadbeef.models;

import javafx.scene.canvas.GraphicsContext;

import java.util.UUID;

/**
 * This class stores the relationships between things in the diagram
 */

public class UmlRelationship {
    private final UUID uuid;
    private UmlThing fromThing;
    private UmlThing toThing;
    private RelationshipType type;

    /**
     * Constructor Default
     */
    public UmlRelationship() {
        this(null, null, RelationshipType.PLAIN);
    }

    /**
     * Constructor. It gives each relationship a random ID number.
     */
    public UmlRelationship(UmlThing thing1, UmlThing thing2, RelationshipType type) {
        this.uuid = UUID.randomUUID();
        this.fromThing = thing1;
        this.toThing = thing2;
        this.type = type;
    }

    public void draw(GraphicsContext context) {
        if (fromThing.getX() < toThing.getX()) {
            double fromX = fromThing.getX() + fromThing.getWidth(), fromY = fromThing.getY();
            double toX = toThing.getX(), toY = toThing.getY();
            context.strokeLine(fromX, fromY, toX, toY);
        }
        else {
            double fromX = fromThing.getX(), fromY = fromThing.getY();
            double toX = toThing.getX() + toThing.getWidth(), toY = toThing.getY();
            context.strokeLine(fromX, fromY, toX, toY);
        }
    }

    /**
     * Sets the parent thing of the relationship
     * @param thing parent thing
     */
    public void setFromThing(UmlThing thing) {
        this.fromThing = thing;
    }

    /**
     * Sets the child thing of the relationship
     * @param thing child thing
     */
    public void setToThing(UmlThing thing) {
        this.toThing = thing;
    }

    /**
     * Returns the type of the relationship
     * @return
     */
    public RelationshipType getType() {
        return type;
    }

    /**
     * Returns the ID of the relationship
     * @return
     */
    public UUID getUUID() {
        return uuid;
    }

    public boolean valid() {
        return toThing != null && fromThing != null;
    }

    /**
     * Returns the child thing of the relationship
     * @return
     */
    public UmlThing getToThing() {
        return toThing;
    }

    /**
     * Returns the parent thing of the relationship
     * @return
     */
    public UmlThing getFromThing() {
        return fromThing;
    }
}
