package deadbeef.models;

import javafx.scene.canvas.GraphicsContext;
import java.util.UUID;

/**
 * This is the base class for all Things in the diagram.
 */
public abstract class UmlThing {

    private final UUID id;
    private String label = null;
    private double height;
    private double width;
    private double xPosition;
    private double yPosition;

    /**
     * Stores the label and dimensions of a thing
     * @param label label for the thing
     * @param width width of the thing
     * @param height height of the thing
     */
    public UmlThing(String label, double width, double height) {
        // set id here by default,
        // get default label and size from the classes that invoke UmlThing()
        this.id = UUID.randomUUID();
        this.label = label;
        this.width = width;
        this.height = height;
    }


    /**
     * This method takes in an X and a Y coordinate and sets them as
     * the x and y position for the object that is being created.
     * @param x
     * @param y
     */
    public void setPosition(double x, double y) {
        setXPosition(x);
        setYPosition(y);
    }

    /**
     * Sets the x position to the parameter that is passed in.
     * @param x
     */
    public void setXPosition(double x) {
        xPosition = x;
    }

    /**
     * Sets the y position to the parameter that is passed in.
     * @param y
     */
    public void setYPosition(double y) {
        yPosition = y;
    }

    /**
     * Returns the x postion.
     * @return xPosition
     */
    public double getX() {
        return xPosition;
    }

    /**
     * Returns the y position.
     * @return yPosition
     */
    public double getY() {
        return yPosition;
    }

    /**
     * Sets the height of the object being created to the
     * parameter that is passed in.
     * @param height
     */
    public void setHeight(double height) {
        this.height = height;
    }

    /**
     * Returns the height that currently set.
     * @return height
     */
    public double getHeight() {
        return height;
    }

    /**
     * Sets the width of the object being created to the
     * parameter that is passed in.
     * @param width
     */
    public void setWidth(double width) {
        this.width = width;
    }

    /**
     * Returns the width that is currently set.
     * @return width
     */
    public double getWidth() {
        return width;
    }

    /**
     * Sets a label for the object that is currently being
     * created.
     * @param label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Returns the current label on the object.
     * @return label
     */
    public String getLabel() {
        return label;
    }

    /**
     * Returns the random ID number that is generated in
     * UmlThing.
     * @return id
     */
    public UUID getUUID() {
        return id;
    }

    public abstract void draw(GraphicsContext gc);

    /**
     * Checks if the user clicks on a thing
     * @param clickX x position of this click within the canvas
     * @param clickY y position of this click within the canvas
     * @return true if the click is bound by this object
     */
    public boolean clickAffectsThisObject(double clickX, double clickY) {
        return clickX >= getX() && clickX <= getX() + getWidth() &&
            clickY >= getY() && clickY <= getY() + getHeight();
    }
}
