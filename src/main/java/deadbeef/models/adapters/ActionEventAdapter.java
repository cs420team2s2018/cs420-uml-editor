package deadbeef.models.adapters;

import com.google.gson.*;
import deadbeef.Main;
import deadbeef.actions.ActionEvent;

import java.lang.reflect.Type;

/**
 *
 * Based on code from:
 * https://stackoverflow.com/a/8683689/4335488
 */
public class ActionEventAdapter implements JsonSerializer<ActionEvent>, JsonDeserializer<ActionEvent> {
    private static final String CLASSNAME = "CLASSNAME";
    private static final String INSTANCE = "INSTANCE";

    /**
     * Deserializes the json object into the appropriate uml equivalents
     * @param json json object to be deserialized
     * @param typeOfT type of action event
     * @param context context for deserialization
     * @return
     * @throws JsonParseException
     */
    @Override
    public ActionEvent deserialize(JsonElement json, Type typeOfT,
                                   JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        JsonPrimitive prim = (JsonPrimitive) jsonObject.get(CLASSNAME);
        String className = prim.getAsString();

        Class<?> klass = null;
        try {
            klass = Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new JsonParseException(e.getMessage());
        }

        return context.deserialize(jsonObject.get(INSTANCE), klass);
    }

    /**
     * Serializes all action events into a single json object
     * @param src list of action events to be serialized
     * @param typeOfSrc type of action event
     * @param context context for serialization
     * @return
     */
    @Override
    public JsonElement serialize(ActionEvent src, Type typeOfSrc,
                                 JsonSerializationContext context) {
        JsonObject retValue = new JsonObject();
        String className = src.getClass().getName();
        retValue.addProperty(CLASSNAME, className);
        JsonElement elem = context.serialize(src);
        retValue.add(INSTANCE, elem);
        return retValue;
    }
}
