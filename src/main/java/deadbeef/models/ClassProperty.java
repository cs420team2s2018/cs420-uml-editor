package deadbeef.models;

/**
 * Class property data class
 */
public final class ClassProperty {
   private String name;
   private String type;


   /**
    * Constructs with a name and type
    */
   public ClassProperty (String name, String type) {
      this.name = name;
      this.type = type;
   }

    /**
     * Default blank constructor
     */
   public ClassProperty() {
      this("", "");
   }

    /**
     * Returns the class details formatted
     *
     * @return A formatted string describing the details of this property
     *
     * If name is empty: the empty string
     * If type is empty: just the name
     * Otherwise: "%{name}: %{type}
     *
     * Whitespace is always trimed from the type and name
     */
   public String getFormatted() {
      if (name.isEmpty()) {
          return "";
      } else if (type.isEmpty()) {
          return name.trim();
      } else {
          return name.trim() + ": " + type.trim();
      }
   }

    /**
     * Returns the name of the class property
     * @return
     */
   public final String getName() {
       return name;
   }

    /**
     * Returns the type of the class property
     * @return
     */
   public final String getType() {
       return type;
   }
}
