package deadbeef.models;


import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class UmlUseCase  extends UmlThing {


    /**
     * Specifies label, width, and height of the use case being
     * created. Also prints out a message stating the use case
     * has been created.
     */
    public UmlUseCase() {
        super("new usecase", 200, 50);
        System.out.println("new use case created");

    }

    /**
     * Draws the use case in the given context
     * @param context
     */
    public void draw(GraphicsContext context) {
        context.setStroke(Color.BLACK);
        context.strokeOval(getX(), getY(), getWidth(), getHeight());
        context.strokeText(getLabel(), getX() + getWidth() * .25, getY(),
                getWidth() * .5);
    }
}
