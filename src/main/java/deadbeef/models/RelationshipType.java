package deadbeef.models;

/**
 * Type of Relationship
 */
public enum RelationshipType {
    PLAIN,
    DEPENDANCY,
    ASSOCIATION,
    AGGREGATION,
    COMPOSITION,
    GENERALIZATION
}
