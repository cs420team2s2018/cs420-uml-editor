package deadbeef.models;

import java.util.ArrayList;
import java.util.Collection;


/**
 * Class method data class
 */
public class ClassMethod {

    // contains methods defined by ClassProperty
    // stores the name of a defined method, the return type of the method, and the parameters of the method

    private String name;
    private String returnType;
    private ArrayList<ClassProperty> parameters;

    /**
     * Creates a new empty class method
     */
    public ClassMethod() {
        this("", "");
    }

    /**
     * Creates a new class method and assigns its name and return type
     * @param name name of the class method
     * @param returnType return type of the class method
     */
    public ClassMethod(String name, String returnType) {
        this.name = name;
        this.returnType = returnType;
        this.parameters = new ArrayList<>();
    }

    /**
     * Adds parameters to the class method using one or more class properties
     * @param param class property parameter to be added to the class method
     */
    public void addParameter(ClassProperty param) {
        parameters.add(param);
    }

    /**
     *
     * A formatted string consists of the following spec
     *
     *      %{name}(%{', '.join(map(param.getFormatted, params))}): %{returnType}
     *
     * @return the formatted string describing this method
     */
    public String getFormatted() {
        StringBuilder sb = new StringBuilder(name);
        if (sb.length() == 0) {
            return "";
        }
        sb.append("(");
        if (parameters.size() > 0) {
            sb.append(parameters.get(0).getFormatted());
            for (int i = 1; i < parameters.size(); ++i){
                sb.append(", ");
                sb.append(parameters.get(i).getFormatted());
            }
        }
        sb.append(")");
        if (this.returnType.length() > 0) {
            sb.append(": ");
            sb.append(returnType);
        }
        return sb.toString();
    }

}
