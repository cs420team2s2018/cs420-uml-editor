package deadbeef.models;

import javafx.scene.canvas.GraphicsContext;

import java.util.*;

/**
 * This class stores the current state of the model.
 * Stores items from the type hierarchy of UmlThing and UmlRelationship
 *
 * It is acted on by ActionEvent
 */
public class DiagramModel {

    private TreeMap<UUID, UmlThing> thingCollection;
    private TreeMap<UUID, UmlRelationship> relationshipCollection;

    /**
     * Create an empty model container
     */
    public DiagramModel() {
        thingCollection = new TreeMap<>();
        relationshipCollection = new TreeMap<>();
    }

    /**
     * Returns the ID of a thing
     * @param id ID of the thing
     * @return
     */
    public UmlThing getThingById(UUID id) {
        return thingCollection.get(id);
    }

    /**
     * Adds a thing to the diagram model container
     * @param thing thing to be added
     */
    public void addThing(UmlThing thing) {
        thingCollection.put(thing.getUUID(), thing);
    }

    /**
     * Removes a thing from the diagram model container
     * @param id ID of the thing to be removed
     */
    public void removeThingById(UUID id) {

        thingCollection.remove(id);
    }

    /**
     * Adds a relationship to the diagram model container
     * @param relationship relationship to be added
     */
    public void addRelationship(UmlRelationship relationship) {
        relationshipCollection.put(relationship.getUUID(), relationship);
    }

    /**
     * Removes a relationship from the diagram model container
     * @param id ID of the relationship to be removed
     */
    public void removeRelationshipById(UUID id) {
        relationshipCollection.remove(id);
    }

    /**
     * Issues a draw call to canvas using a buffer. This method pushes the
     * parameters onto the buffer where they will be rendered.
     * @param context
     */
    public void draw(GraphicsContext context) {
        for (UmlThing entry : thingCollection.values()) {
            entry.draw(context);
        }
        for (UmlRelationship entry : relationshipCollection.values()) {
            entry.draw(context);
        }
    }

    /**
     * Returns the total number of things in the diagram model
     * @return size of thingCollection
     */
    public int getThingCount() {
        return thingCollection.size();
    }

    /**
     * Returns the total number of relationships in the diagram model
     * @return size of relationshipCollection
     */
    public int getRelationshipCount() {
        return relationshipCollection.size();
    }

    /**
     * Returns the thing that was clicked on by the user
     * @param xPos x-coordinate of thing
     * @param yPos y-coordinate of thing
     * @return
     */
    public UmlThing getThingClickAffects(double xPos, double yPos) {
        for(UmlThing entry : thingCollection.values()) {
            if (entry.clickAffectsThisObject(xPos, yPos)) {
                System.out.println(entry);
                return entry;
            }
        }
        return null;
    }

    /**
     * Returns the values of the relationship collection
     * @return
     */
    public Collection<UmlRelationship> getRelationshipSet() {
        return relationshipCollection.values();
    }
}
