package deadbeef.models;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import deadbeef.actions.ActionEvent;
import deadbeef.models.adapters.ActionEventAdapter;
import javafx.scene.canvas.GraphicsContext;

import java.io.File;
import java.util.Stack;

public class Diagram {
    private transient DiagramModel model = new DiagramModel();
    private File file;
    private Stack<ActionEvent> currentActions;
    private Stack<ActionEvent> revertedActions;
    private static Gson gson = new GsonBuilder()
            .setPrettyPrinting()
            .registerTypeAdapter(ActionEvent.class, new ActionEventAdapter())
            .create();

    /**
     * Contains one stack structure for executing events on the current
     * diagram. Contains a second stack structure for undoing events
     * on the current diagram.
     */
    public Diagram() {
        currentActions = new Stack<>();
        revertedActions = new Stack<>();
        file = null;
    }

    /**
     * Builds a Diagram from a piece of json produced via gson
     * @param json
     * @return new diagram object
     */
    public static Diagram fromJson(String json) {
        Diagram d = gson.fromJson(json, Diagram.class);
        d.replayActionEvents();
        return d;
    }


    /**
     * Replaces the diagram model with a new one, then replays all events in the
     * currentAction stack. This helps with loading from a file, as GSON does not
     * use the class interface of performAction for doing things to the model.
     */
    private void replayActionEvents() {
        model = new DiagramModel();
        for (ActionEvent e : currentActions) {
            e.perform(model);
        }
    }


    /**
     * Manages the actions applied to the diagram model. If the revertedActions
     * stack has anything inside it, the stack is cleared. If an event occurs
     * on the diagram, it is pushed into the currentActions stack.
     * @param event
     */
    public void performAction(ActionEvent event) {
        if (event == null)
            return;
        if (revertedActions.size() > 0) {
            revertedActions.clear();
        }
        event.perform(model);
        currentActions.push(event);
    }


    /**
     * Undos any changes made to the current diagram. Pops the last event added
     * to the currentActions stack and pushes the event onto the revertedActions
     * stack.
     */
    public void undo() {
        if (currentActions.size() > 0) {
            ActionEvent e = currentActions.peek();
            e.revert(model);
            currentActions.pop();
            revertedActions.push(e);
        }
    }


    /**
     * Redos any action that was previously undone on the diagram. If the
     * revertedActions stack has anything inside it, it pops that action from
     * the revertedActions stack and pushes that event to the currentActions stack.
     */
    public void redo() {
        if (revertedActions.size() > 0) {
            ActionEvent e = revertedActions.peek();
            e.perform(model);
            currentActions.push(e);
            revertedActions.pop();
        }
    }

    /**
     * @return the serialized object as a JSON string
     */
    public String serializeToJSON() {
        return gson.toJson(this);
    }

    /**
     * @return the number of actions currently affecting the model
     */
    public int getCurrentActionCount() {
        return currentActions.size();
    }

    /**
     * @return the number of actions currently in the reverted stack
     */
    public int getRevertedActionCount() {
        return revertedActions.size();
    }

    public void draw(GraphicsContext context) {
        model.draw(context);
    }

    /**
     * Sets the file to save to
     * @param file
     */
    public void setFile(File file) {
        this.file = file;
    }

    public File getFile(){
        return file;
    }

    /**
     *
     * @return the underlying model
     */
    public DiagramModel getModel() {
        return model;
    }

    public UmlThing getThingClickAffects(double xPos, double yPos) {
        return model.getThingClickAffects(xPos, yPos);
    }
}
