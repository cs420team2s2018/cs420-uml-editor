package deadbeef.controllers;

import deadbeef.controllers.handlers.CanvasClickEventHandler;
import deadbeef.controllers.handlers.MainUiState;
import deadbeef.models.Diagram;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static javafx.scene.input.MouseEvent.MOUSE_CLICKED;


/** Controls the scene
 *
 */
public class MainController {
    @FXML
    private MenuItem m_new_diagram;
    @FXML
    private MenuItem m_open_file;
    @FXML
    private ScrollPane img_pane;
    @FXML
    private Canvas canvas;
    // master diagram object
    private Diagram diagram = new Diagram();
    private CanvasClickEventHandler canvasEventHandler;
    private Stage stage;
    private final FileChooser fileChooser = new FileChooser();



    private class CreateNewDiagramHandler implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent event) {
            diagram = new Diagram();
            redraw();
        }
    }

    public void initialize() {
        canvasEventHandler = new CanvasClickEventHandler(this);
        canvas.addEventHandler(MOUSE_CLICKED, canvasEventHandler);

        img_pane.setHbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        img_pane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        img_pane.setPannable(true);

        img_pane.hvalueProperty().setValue(0.5);
        img_pane.vvalueProperty().setValue(0.5);


        InvalidationListener sizeChange = new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                System.out.println("Canvas Size Change");
                redraw();
            }
        };
        canvas.widthProperty().addListener(sizeChange);
        canvas.heightProperty().addListener(sizeChange);

        m_open_file.setOnAction(new OpenFileHandler());
        m_new_diagram.setOnAction(new CreateNewDiagramHandler());
    }

    private void warnBadFile() {
        //TODO: Implement a warning if a bad file is selected
    }

    /**
     * Sets the file of the diagram
     */
    @FXML
    private void saveFileAs() {
        File file = fileChooser.showSaveDialog(stage);
        if (file != null) {
            diagram.setFile(file);
            saveFile();
        }
    }

    @FXML
    private void saveFile() {
        File file = diagram.getFile();
        if (file == null) {
            file = fileChooser.showSaveDialog(stage);
            diagram.setFile(file);
        }
        try {
            String output = diagram.serializeToJSON();
            BufferedWriter writer = new BufferedWriter(new FileWriter(diagram.getFile()));
            writer.write(output);
            writer.close();
        } catch (IOException except) {
            //TODO: DO Nothing, not much we can do
        }
    }

    /**
     * Notify the create handler to create a new class box on next click in canvas
     * @param event
     */
    @FXML
    private void handleClassBoxButtonAction(ActionEvent event) {
        canvasEventHandler.setUiState(MainUiState.CREATE_CLASS_BOX);
    }

    /**
     * Notify the create handler to create a new use case on next click in canvas
     * @param event
     */
    @FXML
    private void handleUseCaseAction (ActionEvent event) {
        canvasEventHandler.setUiState(MainUiState.CREATE_USE_CASE);
    }

    @FXML
    private void handleDeleteThingPress (ActionEvent event) {
        canvasEventHandler.setUiState(MainUiState.DELETE_THING);
    }

    @FXML
    private void showHelp(ActionEvent e) {

    }

    /**
     * Sets the stage to manipulate
     * @param stage
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }
    
    @FXML
    private void showVersion(ActionEvent e) {
        Alert version = new Alert(Alert.AlertType.INFORMATION);
        version.setTitle("0xDEADBEEF UML Editor Version");
        version.setHeaderText("0xDEADBEEF UML EDITOR Iteration 3");
        version.setContentText(
                "(c) 2018 All Rights Reserved\n\n" +
                "Created By:\n" +
                "Henry J Schmale\n" +
                "Joe Thompson\n"+
                "Gina Ile\n" +
                "Casey Jo McCarter\n" +
                "Nick Bulisky\n\n" +
                "Created for Millersville University csci420 Spring 2018"
        );

        version.showAndWait();
    }

    /**
     * Triggers the redraw of the canvas
     */
    public void redraw() {
        GraphicsContext ctx = canvas.getGraphicsContext2D();
        ctx.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        diagram.draw(ctx);
    }

    /**
     * Handles the undo event
     */
    @FXML
    private void handleUndo() {
        diagram.undo();
        redraw();
    }

    /**
     * Handles the redo event
     */
    @FXML
    private void handleRedo() {
        diagram.redo();
        redraw();
    }

    /**
     * Should bring up a system print dialog and send the canvas to the printer
     * @param event
     */
    @FXML
    void printDiagram(ActionEvent event) {
        System.out.println("PRINT MENU PRESSED");
    }

    /**
     * Triggers the exit of the application
     * @todo Prompt the user to save if there's been changes since last saved
     * @param event
     */
    @FXML
    void quitApp(ActionEvent event) {
        System.out.println("QUIT PRESSED");
        Platform.exit();
    }

    @FXML
    void handleThingMovePress(ActionEvent event) {
        canvasEventHandler.setUiState(MainUiState.MOVE_THING);
    }

    private class OpenFileHandler implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent event) {
            File file = fileChooser.showOpenDialog(stage);
            if (file != null) {
                try {
                    String loadedJson = new String(
                            Files.readAllBytes(Paths.get(file.getPath())),
                            StandardCharsets.UTF_8);
                    diagram = Diagram.fromJson(loadedJson);
                    redraw();
                } catch (IOException e) {
                    warnBadFile();
                }
            }
        }
    }

    @FXML
    void handleRelationshipCreate() {
        canvasEventHandler.setUiState(MainUiState.CREATE_RELATIONSHIP);
    }


    /**
     * DO NOT TRY TO SAVE A REFERENCE TO THIS. IT WILL CHANGE. IT DOES NOT RETURN AN OBSERVER
     * @return the current diagram object
     */
    public Diagram getDiagram() {
        return diagram;
    }
}





