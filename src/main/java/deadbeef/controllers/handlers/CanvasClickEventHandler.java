package deadbeef.controllers.handlers;

import deadbeef.actions.*;
import deadbeef.controllers.MainController;
import deadbeef.models.UmlThing;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

import static deadbeef.controllers.handlers.MainUiState.*;


public class CanvasClickEventHandler implements EventHandler<MouseEvent> {
    private MainController controller;
    private MainUiState uiState;
    private ActionEvent storedEvent;

    public CanvasClickEventHandler(MainController d) {
        this.controller = d;
        this.uiState = MainUiState.NONE;
    }

    @Override
    public void handle(MouseEvent event) {
        ActionEvent e = null;
        switch(uiState) {
            case CREATE_CLASS_BOX:
            case CREATE_USE_CASE:
                handleCreateThings(event);
                break;
            case CREATE_RELATIONSHIP:
            case PRIVATE_CREATE_RELATIONSHIP_SELECT_OBJECT_2:
                handleRelationshipCreate(event);
                break;
            case DELETE_THING:
                handleDeleteThing(event);
                break;
            case MOVE_THING:
            case PRIVATE_MOVE_THING_NEW_POSITION:
                handleMoveThing(event);
                break;
        }
    }

    private void handleRelationshipCreate(MouseEvent event) {
        UmlThing thing = getThingClick(event);
        ActionCreateRelationship action;
        if (thing == null) {
            resetUiState();
        }
        if (uiState == CREATE_RELATIONSHIP) {
            action = new ActionCreateRelationship();
            action.setFromThing(thing);
            storedEvent = action;
            uiState = PRIVATE_CREATE_RELATIONSHIP_SELECT_OBJECT_2;
        } else if (uiState == PRIVATE_CREATE_RELATIONSHIP_SELECT_OBJECT_2) {
            action = (ActionCreateRelationship) storedEvent;
            action.setToThing(thing);
            commitActionToDiagram(action);
        }
    }

    private void handleDeleteThing(MouseEvent event) {
        UmlThing thing = getThingClick(event);
        if (thing != null)
            commitActionToDiagram(new ActionDeleteUmlThing(thing));
    }

    private void handleCreateThings(MouseEvent event) {
        ActionEvent e = null;
        if (uiState == CREATE_CLASS_BOX)
            e = new ActionCreateClassBox(event.getX(), event.getY());
        else if (uiState == CREATE_USE_CASE)
            e = new ActionCreateUseCase(event.getX(), event.getY());

        if (e != null)
            commitActionToDiagram(e);
    }

    private void handleMoveThing(MouseEvent event) {
        if (uiState == MOVE_THING) {
            UmlThing thing = getThingClick(event);
            if (thing != null) {
                storedEvent = new ActionMoveThing(thing);
                uiState = MainUiState.PRIVATE_MOVE_THING_NEW_POSITION;
            }
        } else if (uiState == PRIVATE_MOVE_THING_NEW_POSITION){
            ActionMoveThing me = (ActionMoveThing) storedEvent;
            me.setNewPosition(event.getX(), event.getY());
            commitActionToDiagram(me);
        }
    }

    /**
     * handles click and returns the thing it affects
     * @param event
     * @return thing affected
     */
    private UmlThing getThingClick(MouseEvent event) {
        return controller.getDiagram()
                .getThingClickAffects(event.getX(), event.getY());
    }

    private void commitActionToDiagram(ActionEvent e) {
        controller.getDiagram().performAction(e);
        controller.redraw();
        uiState = MainUiState.NONE;
        storedEvent = null;
    }

    /**
     * Set the add thing interaction
     * @param state
     */
    public void setUiState(MainUiState state) {
        this.uiState = state;
        this.storedEvent = null;
    }

    public void resetUiState() {
        setUiState(NONE);
    }
}
