package deadbeef.controllers.handlers;

/**
 * States that the UI can be in for editing the canvas
 *
 * Used by CanvasClickEventHandler to determine what to do on a click event
 */
public enum MainUiState {
    /**
     * User wishes to create a thing
     */
    CREATE_CLASS_BOX,
    CREATE_USE_CASE,

    /**
     * No Interaction required on mouse event
     */
    NONE,

    /**
     * Users wishes to delete an object
     */
    DELETE_THING,

    /**
     * User Wishes TO Move a Thing
     */
    MOVE_THING,

    /**
     * User wishes to create a relationship between two objects
     */
    CREATE_RELATIONSHIP,

    PRIVATE_CREATE_RELATIONSHIP_SELECT_OBJECT_2,

    /**
     * State Transitioned To From MOVE_THING
     * 2nd Click
     */
    PRIVATE_MOVE_THING_NEW_POSITION
}
